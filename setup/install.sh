#!/bin/bash
# This is the entry point for configuring the system.
############################################

#install basic tools
apt-get install -y dkms g++ gcc gdb git iproute2 make net-tools python vim pkg-config zip zlib1g-dev unzip libibverbs1 ibverbs-utils libibverbs-dev librdmacm1 rdmacm-utils librdmacm-dev automake m4 libtool uuid-dev libnuma-dev libaio-dev cscope exuberant-ctags

#install Bazel
mkdir -p /tmp/bazel
cd /tmp/bazel
/bin/rm -rf *
wget https://github.com/bazelbuild/bazel/releases/download/0.19.1/bazel-0.19.1-installer-linux-x86_64.sh
chmod +x bazel-0.19.1-installer-linux-x86_64.sh
./bazel-0.19.1-installer-linux-x86_64.sh

#install golang
wget https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz
tar -xvf go1.11.4.linux-amd64.tar.gz
mv go /usr/local
rm go1.11.4.linux-amd64.tar.gz

#set up the golang path
touch /home/vagrant/.bash_profile
echo "export PATH=$PATH:/usr/local/go/bin" >> /home/vagrant/.bash_profile
echo "export GOPATH=/home/vagrant/ws/go" >> /home/vagrant/.bash_profile
echo "export GOROOT=/usr/local/go/bin" >> /home/vagrant/.bash_profile
mkdir -p "$GOPATH/bin"
