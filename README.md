## What is Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.

---

## Learn Vagrant

You can learn about how to use Vagrant from https://www.vagrantup.com/intro/getting-started/index.html

---

## An example to use Vagrant with VirtualBox on MacOS

1. Download and install Oracle VirtualBox.
2. Download and install Vagrant from www.vagrantup.com.
3. Open a Terminal on Mac.
4. Goto the directory where you will start Vagrant and copy over the file "Vagrantfile".
5. Create directory "workspace/src" where the project codes will be located.
6. On the same directory, create a new directory named as "setup" and copy the file "install.sh" to the new directory.
7. Run "vagrant up" which will create the VM if it is the first time and starts the VM.
8. Run "vagrant ssh" to log into the VM.
9. Now you can build and run the project from /home/vagrant/workspace/src.
